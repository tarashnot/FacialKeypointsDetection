filter_img <- function(data = training[, 31:9246], number = FALSE, vis_image = FALSE, th = -50, func = 1){
        #Gaussian Laplacian filter
        
        #data - data.frame object each row of which contains 9216 columns - 96x96 image pixels intensity
        #number - integer identifies image to filter, should be in range 1:nrow(data)
        #vis_image - should image filtered image be visualized
        #th - threshold for gaussian laplacian filter (all filtered values that less than th will be truncated to 0)
        #func - type of filter matrix
        
        #array, which contains pixels for filtered image
        
        if (number == FALSE) {
                number <- sample(1:nrow(data), 1)
        }
        
        face <- matrix(unlist(data[number, ]), ncol = 96, nrow = 96)
        
        if (func == 1) {
                fHigh1 <- matrix(c(0, -1, 0, -1, 4, -1, 0, -1, 0), nc = 3, nr = 3, byrow = T) 
        } else {
                fHigh1 <- matrix(c(0.17, 0.67, 0.17, 0.67, -2.33, 0.67, 0.17, 0.67, 0.17), nc = 3, nr = 3, byrow = T)        
        }
        
        face_tr = matrix(NA, ncol = 94, nrow = 94)
        
        for (i in 2:95) {
                for (j in 2:95) {
                        face_tr[i - 1, j - 1] <- sum(face[(i-1):(i+1), (j-1):(j+1)] * fHigh1)
                }
        }
        
        face_tr[face_tr < th] <- 0
        face_tr <- cbind(rep(0, 96), rbind(rep(0, 94), face_tr, rep(0, 94)), rep(0, 96))
        
        if (vis_image == TRUE) {
                image(1:96, 1:96, face_tr, col = grey(seq(0, 1, length = 256)), ylim = c(94,1), xlab = "", ylab = "")
        }
        
        return(c(face_tr))
}