# find_part(C50, rf) Investigation
azhubryd  
15 серпня 2015 р.  


```r
### Loading and preprocessing
# load("D:/Copy/found_part_1.RData")
# load("D:/Copy/found_part_2.RData")
# load("D:/Copy/found_part_3.RData")
# load("D:/Study/Kaggle/Facial/FacialKeypointsDetection/Data/data_noNA/data_keypoint.RData")
# load("D:/Study/Kaggle/Facial/FacialKeypointsDetection/Data/data_noNA/data_train.RData")
# library(data.table)
# library(dplyr)
# found_parts <- list(
#     found_part_1,
#     found_part_2,
#     found_part_3
# ) %>% rbindlist() %>% 
#     arrange(ID, model, -x) %>% 
#     mutate(keypoint = c('left_eye_center','right_eye_center')) %>%
#     mutate(keypoint = factor(keypoint))
# rm(found_part_1, found_part_2, found_part_3)
# save.image('Investigation_env.RData')

# Loading previously preprocessed
# Prediction res=c(10,6)
res=c(10,6,10,6)
library(facial)
```

```
## Loading required package: dplyr
## 
## Attaching package: 'dplyr'
## 
## The following object is masked from 'package:stats':
## 
##     filter
## 
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
## 
## Loading required package: ggplot2
## Loading required package: data.table
## 
## Attaching package: 'data.table'
## 
## The following objects are masked from 'package:dplyr':
## 
##     between, last
```

```r
load('Investigation_env.RData')

RMSE(found_parts)
```

```
## Source: local data table [6 x 2]
## 
##   model    error
## 1     1 22.87436
## 2     2 18.58747
## 3     3 16.36476
## 4     4 23.03310
## 5     5 16.20082
## 6     6 16.21999
```

```r
RMSE_wogr <- function(find_result, keypoints = data_keypoint, model_sel = 1) {
    keypoints %>% 
        filter(ID %in% unique(find_result$ID) & keypoint %in% unique(find_result$keypoint)) %>% 
        arrange(ID, keypoint) %>%
        merge(find_result %>% filter(model %in% model_sel), by = c('ID', 'keypoint')) %>% 
        mutate(diff = (x.x-x.y)^2 + (y.x-y.y)^2)
}

# model_sel:
#   1 - C5.0
#   2 - rf
topped <- RMSE_wogr(found_parts,model_sel = 2) %>% 
    group_by(ID) %>% 
    summarise(diff = sum(diff)) %>%
    arrange(-diff)
quantile(topped$diff)
```

```
##           0%          25%          50%          75%         100% 
## 1.125018e-02 1.134602e+00 6.364263e+00 1.496192e+03 6.412925e+03
```

```r
lowerIDs <- topped$ID[topped$diff < 2]
midIDs <- sample(topped$ID[topped$diff > 50 & topped$diff < 100],4)
topIDs <- sample(topped$ID[topped$diff>1000], 4)

RMSE(found_parts[ID %in% lowerIDs])
```

```
## Source: local data table [6 x 2]
## 
##   model      error
## 1     1 18.6854946
## 2     2  0.6193491
## 3     3  4.3340730
## 4     4 18.4081345
## 5     5  4.2501027
## 6     6  4.1399270
```

```r
RMSE(found_parts[ID %in% midIDs])
```

```
## Source: local data table [6 x 2]
## 
##   model     error
## 1     1 22.250063
## 2     2  5.993685
## 3     3 14.110469
## 4     4 22.427383
## 5     5 14.604187
## 6     6 14.481103
```

```r
RMSE(found_parts[ID %in% topIDs])
```

```
## Source: local data table [6 x 2]
## 
##   model    error
## 1     1 27.26824
## 2     2 34.19869
## 3     3 32.30499
## 4     4 27.29575
## 5     5 26.51980
## 6     6 26.51980
```

```r
visual_ggplot(faceID = topIDs) + 
    geom_rect(data = RMSE_wogr(found_parts,model_sel = 1:2)[ID %in% topIDs],
               mapping = aes(xmin=x.y-res[1], ymin=y.y-res[4], 
                             xmax=x.y+res[3], ymax=y.y+res[2], 
                             color = model),
              inherit.aes = FALSE, alpha=0.1) +
    theme(legend.position = 'right')
```

![](readme_files/figure-html/unnamed-chunk-1-1.png) 

```r
lowerIDs4 <- sample(lowerIDs, 4)
visual_ggplot(faceID = lowerIDs4) + 
    geom_rect(data = RMSE_wogr(found_parts,model_sel = 1:2)[ID %in% lowerIDs4],
              mapping = aes(xmin=x.y-res[1], ymin=y.y-res[4], 
                            xmax=x.y+res[3], ymax=y.y+res[2], 
                            color = model),
              inherit.aes = FALSE, alpha=0.1) +
    theme(legend.position = 'right')
```

![](readme_files/figure-html/unnamed-chunk-1-2.png) 

```r
visual_ggplot(faceID = midIDs) + 
    geom_rect(data = RMSE_wogr(found_parts,model_sel = 1:2)[ID %in% midIDs],
              mapping = aes(xmin=x.y-res[1], ymin=y.y-res[4], 
                            xmax=x.y+res[3], ymax=y.y+res[2], 
                            color = model),
              inherit.aes = FALSE, alpha=0.1) +
    theme(legend.position = 'right')
```

![](readme_files/figure-html/unnamed-chunk-1-3.png) 
