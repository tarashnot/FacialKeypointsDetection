## Requires training_no_NA to be loaded ##

# Loading packages 
library(dplyr)
library(reshape2)
library(data.table)
library(tidyr)

load('environment.RData')
training_no_NA <- training
# Add id's to the rows of no_NA dataset
training_no_NA$ID <- as.numeric(row.names(training_no_NA))


# Function for transformation of training set
# Uses dplyr and reshape2 libraries
# Output of the function is a long frame with x, y, value and ID
getit <- function(input_data = training_no_NA[1,]) {
    melt(t(matrix(as.numeric(input_data[31:9246]), nrow = 96, ncol = 96))) %>%
    tbl_df() %>%
    mutate(y = 97-Var1, x = Var2, ID = input_data[9247]) %>%
    select(ID, x, y, value)
}

# Creating a long training data
# texture only! without recognized points
data_train <- data.table(rbindlist(
    apply(training_no_NA, 1, function(x) getit(as.numeric(x)))
    ))
data_1 <- training_no_NA[1:2000,] %>%
    apply(1, function(x) getit(as.numeric(x))) %>%
    rbindlist()
data_2 <- training_no_NA[2001:4000,] %>%
    apply(1, function(x) getit(as.numeric(x))) %>%
    rbindlist()
data_3 <- training_no_NA[4001:6000,] %>%
    apply(1, function(x) getit(as.numeric(x))) %>%
    rbindlist()
data_4 <- training_no_NA[6001:7046,] %>%
    apply(1, function(x) getit(as.numeric(x))) %>%
    rbindlist()
#save(training_no_NA, file = 'old_training_full.RData')
#save(data_1, file = 'data_1.RData')
load('old_training_full.RData')
load('data_1.RData')
load('data_2.RData')
load('data_3.RData')
load('data_4.RData')
data_train <- list(data_1, data_2, data_3, data_4) %>% 
    rbindlist() %>% data.table(key ='ID')


save(data_train, file = 'Data/data_train.RData')

# Recognized points data
data_keypoint <- gather(training_no_NA[,c(1:30,9247)],variable, value, -ID) 
data_keypoint <- data_keypoint %>% 
    separate(col = 'variable',into = c('keypoint', 'axis'),sep = '(?<=.)_(?=.{1}$)',remove = TRUE) %>%
data_keypoint <- data_keypoint %>%     spread(axis, value) %>%
    data_keypoint <- data_keypoint %>%     mutate(x = 48*(1+x), y = 48*(1-y)) %>%
    
data_keypoint <- data_keypoint %>% mutate(y = 96-y)
data_keypoint <- data_keypoint %>%     data.table(key = 'ID')
save(data_keypoint, file = 'FacialKeypointsDetection/Data/data_keypoint_full.RData')
